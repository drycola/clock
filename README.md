# clock

## Setup
Install java 11

```bash
brew tap caskroom/versions
brew cask install java11

echo "export JAVA_HOME=/Library/Java/JavaVirtualMachines/openjdk-11.0.1.jdk/Contents/Home" >> ~/.bash_profile

source ~/.bash_profile
```

* If using a different version of java upate  gradle package version

```vim
vim gradle/gradel-wrapper.properties

distributionUrl=https\://services.gradle.org/distributions/gradle-4.10-all.zip
```

## Run Application

```bash
./gradlew desktop:run
```

## Build Artifact

```bash
 ./gradlew desktop:dist
```

## Configuration

All assets are located in core/assets

```bash
vim core/assets/config/config.json
```

[addtional information](https://github.com/libgdx/libgdx/wiki/Gradle-on-the-Commandline#running-the-desktop-project)
