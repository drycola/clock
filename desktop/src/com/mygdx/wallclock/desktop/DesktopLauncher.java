package com.mygdx.wallclock.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.wallclock.WallClock;

public class DesktopLauncher {
	public static void main (String[] arg) {
		final LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Time Code Generator";
		config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
		config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height;
		config.foregroundFPS = 29;
        config.backgroundFPS = 29;
		config.fullscreen = false;
		config.useHDPI = true;
		//new LwjglApplication(new WallClock(), config);
        new LwjglApplication(new WallClock() {
            @Override
            protected void setFPS(int value) {
                config.foregroundFPS = value;
                config.backgroundFPS = value;
            }
        }, config);
	}
}

