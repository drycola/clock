package com.mygdx.wallclock;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.JsonValue;


public class TextField {
    private BitmapFont text;
    private SpriteBatch batch;
    private int x;
    private int y;
    private int width;
    private int align;
    private boolean valid_path;
    private String error;


    TextField(JsonValue config) {
        text = new BitmapFont();
        align = Align.left;
        valid_path = true;

        try {
            x = config.getInt("x");
        }
        catch (IllegalArgumentException e) {
            x = 100;
            valid_path = false;
            error = e.toString() + "\nNOTE: check config file";
        }

        try {
            y = config.getInt("y");
        }
        catch (IllegalArgumentException e) {
            y = 100;
            valid_path = false;
            error = e.toString() + "\nNOTE: check config file";
        }

        try {
            width = config.getInt("width");
        }
        catch (IllegalArgumentException e) {
            valid_path = false;
            error = e.toString() + "\nNOTE: check config file";
        }

        try {
            if (config.getBoolean("align_center")) {
                align = Align.center;
            }
        }
        catch (IllegalArgumentException e) {
            valid_path = false;
            error = e.toString() + "\nNOTE: check config file";
        }

        try {
            text = new BitmapFont(Gdx.files.internal(config.getString("font_path")));
        }
        catch (IllegalArgumentException e) {
            valid_path = false;
            error = e.toString() + "\nNOTE: check config file";
            text = new BitmapFont();
        }
        catch (GdxRuntimeException e) {
            valid_path = false;
            error = e.getMessage();
            text = new BitmapFont();
        }

        batch = new SpriteBatch();
    }

    public void text(String message) {
        if (!valid_path) {
            message = error;
        }
        batch.begin();
        text.draw(
                batch,
                message,
                x,
                y,
                width,
                align,
                true);
        batch.end();
    }

    public void dispose () {
        batch.dispose();
    }
}
