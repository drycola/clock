package com.mygdx.wallclock;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.JsonValue;

public class Image {
    private SpriteBatch batch;
    private Texture image;
    private int x;
    private int y;
    private int width;
    private int height;
    private boolean valid_path;
    private String error;

    private BitmapFont text;

    Image(JsonValue config) {

        valid_path = true;
        text = new BitmapFont();

        try {
            image = new Texture(config.getString("file_path"));
        }
        catch (IllegalArgumentException e) {
            valid_path = false;
            error = e.toString() + "\nNOTE: check config file";
            text = new BitmapFont();
        }
        catch (GdxRuntimeException e) {
            valid_path = false;
            error = e.getMessage();
            text = new BitmapFont();
        }

        try {
            x = config.getInt("x");
        }
        catch (IllegalArgumentException e) {
            x = 100;
            valid_path = false;
            error = e.toString() + "\nNOTE: check config file";
        }

        try {
            y = config.getInt("y");
        }
        catch (IllegalArgumentException e) {
            y = 100;
            valid_path = false;
            error = e.toString() + "\nNOTE: check config file";
        }

        try {
            width = config.getInt("width");
        }
        catch (IllegalArgumentException e) {
            valid_path = false;
            error = e.toString() + "\nNOTE: check config file";
        }

        try {
            height = config.getInt("height");
        }
        catch (IllegalArgumentException e) {
            valid_path = false;
            error = e.toString() + "\nNOTE: check config file";

        }

        batch = new SpriteBatch();
    }

    public void draw() {
        batch.begin();
        if (valid_path) {
            batch.draw(image,
                    x,
                    y,
                    width,
                    height);
        }
        else {
            text.draw(
                    batch,
                    error,
                    x,
                    y,
                    width,
                    Align.center,
                    true);
        }
        batch.end();
    }

    public void dispose () {
        batch.dispose();
    }
}
