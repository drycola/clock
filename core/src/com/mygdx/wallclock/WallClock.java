package com.mygdx.wallclock;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class WallClock extends ApplicationAdapter {
	private Image logo;
	private Image background;
	private JsonValue config;

	private int frame;

	private TextField utc_clock;
    private TextField local_clock;
    private TextField utc_label;
    private TextField local_label;
    private TextField frame_rate;
    private TextField utc_date;

    private DateFormat local_dateFormat;
    private DateFormat dateFormat;
    private DateFormat utc_dateFormat;

    private long now;

	protected void setFPS(int value) {
	}

	@Override
	public void create () {
        config = new JsonReader().parse(Gdx.files.internal("config/config.json"));
        logo = new Image(config.get("logo"));
        background = new Image(config.get("background"));

		utc_clock = new TextField(config.get("utc_clock"));
        local_clock = new TextField(config.get("local_clock"));
        utc_label = new TextField(config.get("utc_label"));
        local_label = new TextField(config.get("local_label"));
        frame_rate = new TextField((config.get("frame_rate_label")));
        utc_date = new TextField(config.get("utc_date"));

        local_dateFormat = new SimpleDateFormat("HH:mm:ss");
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        utc_dateFormat = new SimpleDateFormat("HH:mm:ss");

        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        utc_dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

		setFPS(config.getInt("frame_rate"));

        if (config.getBoolean("fullscreen")) {
            Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
        }

        if (config.getBoolean("hide_mouse")) {
            Gdx.input.setCursorCatched(true);
        }
	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		frame++;

		// Get current time
        now = System.currentTimeMillis();

		if (Math.round(frame) >= Gdx.graphics.getFramesPerSecond()) {
			frame = 0;
		}

//        if (second == 59)
//        {
//            Gdx.gl.glClearColor(255, 0, 0, 1);
//        }
//        else
//        {
//            Gdx.gl.glClearColor(0, 0, 0, 1);
//        }

		String frameStr = (frame >= 10) ? "" +  frame : "0" +  frame;

        background.draw();
        utc_date.text(dateFormat.format(now));

        utc_clock.text(utc_dateFormat.format(now)+ ":" + frameStr);
        local_clock.text(local_dateFormat.format(now) + ":" + frameStr);

        utc_label.text(config.get("utc_label").getString("label"));
        local_label.text(config.get("local_label").getString("label"));

        frame_rate.text("fps: "+Gdx.graphics.getFramesPerSecond());

        logo.draw();

		//Gdx.graphics.setTitle(""+Gdx.graphics.getFramesPerSecond());
	}

	@Override
	public void dispose () {
	    background.dispose();
        utc_date.dispose();
        utc_clock.dispose();
        local_clock.dispose();
        utc_label.dispose();
        local_label.dispose();
        frame_rate.dispose();
        logo.dispose();
	}
}

